/**
* subdomain
*
* @module      :: Policy
* @description :: Manage subdomains
*
*/
module.exports = function(req, res, next) {
  if (!req.subdomains[0]) return res.notFound();
  req.org = req.subdomains[0];
  next();
};
