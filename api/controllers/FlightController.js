/**
* FlightController
*
* @description :: Server-side logic for managing Flights
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

module.exports = {

	create: function(req, res){
		if (!Array.isArray(req.body.flights)) return res.json({ success : false, error : 'required { flights : [] }' }).status(200);
		Promo.find({ 'owner' : req.session.user.id, 'id' : req.params.id_promo }).exec(function addFlights(err, finded) {
			if (!finded) return res.forbidden('You are not permitted to perform this action.');
			var flights = req.body.flights;
			var start_date = finded[0].start_date.getTime();
			var end_date = finded[0].end_date.getTime();
			var diff = (end_date - start_date) / (flights.length + 1);
			var winner_dates = [];
			for (var i = 0; i < flights.length; i++) {
				flights[i].owner = finded[0].id;
				winner_dates.push(new Date(start_date+(diff * (i+1))));
			}
			Promo.update({ 'id' : finded[0].id }, { 'winner_dates' : winner_dates }).exec(function updatePromo(err, updated){
				if (err) return res.json({ success : false, error : 'no update promo' }).status(200);
				Flight.create(flights).exec(function createFlights(err, created){
					return res.json({ data : created }).status(200);
				});
			});
		});
	},

	getAirports: function(req, res){
		var SabreDevStudio = require('sabre-dev-studio');
		var sabre_dev_studio = new SabreDevStudio({
			client_id:     'V1:31btycz6puq0cgtw:DEVCENTER:EXT',
			client_secret: 'th3IF3Bp',
			uri:           'https://api.test.sabre.com'
		});

		sabre_dev_studio.get('/v1/lists/supported/historical/seasonality/airports', {}, function(err, airports){
			if (err) return res.json({ success : false }).status(200);
			return res.json({ success : true, airports : airports }).status(200);
		});
	}



};
