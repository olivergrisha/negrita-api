/**
* PromoControllerController
*
* @description :: Server-side logic for managing Promocontrollers
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

// `find`, `create`, `update`, and `destroy` actions.
module.exports = {

	find : function(req, res) {
		Promo.find({ owner : req.session.user.id }).populate('flights').exec(function findPromo(err, finded) {
			for (var i = 0; i < finded.length; i++) {
				finded[i].stat_flights = { total : finded[i].flights.length, used : 0 }
				for (var j = 0; j < finded[i].flights.length; j++) {
					if (finded[i].flights[j].winner) finded[i].stat_flights.used++;
				}
				finded[i].flights = undefined;
			}
			res.json({ data : finded }).status(200);
		});
	},

	findOne : function(req, res) {
		Promo.findOne({ owner : req.session.user.id, id : req.params.id_promo }).populate('flights').exec(function findPromo(err, finded) {
			res.json({ data : finded }).status(200);
		});
	},

	create : function(req, res){
		Promo.create({
			owner							: req.session.user.id,
			title 						: req.body.title,
			description 			: req.body.description,
			active 						: req.body.active,
			start_date 				: req.body.start_date,
			end_date 					: req.body.end_date,
			flight_type 			: req.body.flight_type,
			flight_start_date : req.body.flight_start_date,
			flight_end_date 	: req.body.flight_end_date
		}).exec(function createPromo(err, created){
			res.json({ data : created }).status(200);
		});
	},

	uploadImage: function(req, res){
		Promo.findOne({ owner : req.session.user.id, id : req.params.id_promo }).exec(function(err, promo) {
			if (err || !promo) res.json({ success : false }).status(200);
			var uploadFile = req.file('uploadFile');

			uploadFile.upload({ dirname: '../../assets/images'},function onUploadComplete (err, files) {
				if (err) return res.serverError(err);
				console.log(files);
				res.json({status:200, file:files});
			});
		});
	}

};
