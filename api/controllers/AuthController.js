/**
 * Authentication Controller
 *
 * This is merely meant as an example of how your Authentication controller
 * should look. It currently includes the minimum amount of functionality for
 * the basics of Passport.js to work.
 */
var AuthController = {

  /**
   * Log out a user and return them to the homepage
   *
   * Passport exposes a logout() function on req (also aliased as logOut()) that
   * can be called from any route handler which needs to terminate a login
   * session. Invoking logout() will remove the req.user property and clear the
   * login session (if any).
   *
   * For more information on logging out users in Passport.js, check out:
   * http://passportjs.org/guide/logout/
   *
   * @param {Object} req
   * @param {Object} res
   */
  logout: function (req, res) {
    req.logout();

    // mark the user as logged out for auth purposes
    req.session.authenticated = false;

    return res.json({ success: true }).status(200);
  },

  /**
   * Create a authentication callback endpoint
   *
   * This endpoint handles everything related to creating and verifying Pass-
   * ports and users, both locally and from third-aprty providers.
   *
   * Passport exposes a login() function on req (also aliased as logIn()) that
   * can be used to establish a login session. When the login operation
   * completes, user will be assigned to req.user.
   *
   * For more information on logging in users in Passport.js, check out:
   * http://passportjs.org/guide/login/
   *
   * @param {Object} req
   * @param {Object} res
   */
  callback: function (req, res) {

    passport.callback(req, res, function (err, user, challenges, statuses) {

      if (err || !user) {
        try{
          return res.json({ success: false, error: (req.flash('error')[0] ? sails.__(req.flash('error')[0]) : 'Error inesperado [1]') }).status(200);
        } catch(err){
          return res.json({ success: false, error: (req.flash('error')[0] ? req.flash('error')[0] : 'Error inesperado [2]') }).status(200);
        }
      }

      req.login(user, function (err) {
        if (err) {
          return res.json({ success: false, error: 'Error inesperado [3]' }).status(200);
        }

        req.session.user = user;

        // Mark the session as authenticated to work with default Sails sessionAuth.js policy
        req.session.authenticated = true

        // Upon successful login, send the user to the homepage were req.user
        // will be available.
        return res.json({ success: true, user: user }).status(200);
      });
    });
  }

};

module.exports = AuthController;
