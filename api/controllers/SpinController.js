/**
* SpinController
*
* @description :: Server-side logic for managing Spins
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

module.exports = {

	create: function(req, res){
		//Check that exist idFlight in the promo and is available
		Flight.findOne({ 'owner' : req.params.idPromo, 'id' : req.body.idFlight }).exec(function(err, flightFinded) {
			if (err || !flightFinded || flightFinded.winner) return res.forbidden('Not permitted: dont find flight or is not available');
			//Check that this user dont spin before
			Spin.find({ 'dni': req.body.spin.dni, owner: req.params.idPromo }).exec(function dniSearch(err, spinFinded) {
				if (err || spinFinded.length) return res.forbidden('Not permitted: this user has already voted ');
				//Create spin
				req.body.spin.owner = req.params.idPromo;
				Spin.create(req.body.spin).exec(function spinCreate(err, createdSpin) {
					//Get Promo
					Promo.findOne({ 'id': req.params.idPromo }).exec(function promoFinded(err, promo) {
						var now = new Date();
						if (promo.winner_dates[0] <=  now || true) { //is winner
							promo.winner_dates.splice(0, 1);
							//update promo
							promo.save(function promoUpdate(err, updatedPromo) {
								if (err) return res.forbidden('Error inesperado');
								flightFinded.winner = createdSpin.id;
								flightFinded.setToken();
								//update flight
								flightFinded.save(function flightUpdate(err, updatedFlight) {
									if (err) return res.forbidden('Error inesperado');
									//send email
									var nodemailer = require('nodemailer');
									var transporter = nodemailer.createTransport({
										service: 'gmail',
										auth: {
											user: 'olivercoduxe@gmail.com',
											pass: 'sabre2015'
										}
									});
									transporter.sendMail({
										from: 'no-replay@gapiffy.com',
										to: createdSpin.email,
										subject: 'token',
										text: 'your token is '+updatedFlight.token
									});

									return res.json({ 'data' : 'enhorabuena, has ganado', 'flight' : updatedFlight }).status(200);
								});
							});
						}
						else return res.json({ 'data' : 'lo sentimos, no has ganado' }).status(200);
					});
				});
			});
		});
	}

};
