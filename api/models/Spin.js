/**
* Spin.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    owner : { model:'Promo' },

    dni   : { type: 'string', unique: true },

    email : { type: 'email' }
  }
};
