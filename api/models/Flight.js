/**
* Flight.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var md5 = require('MD5');

module.exports = {

  attributes: {

    owner          : { model:'Promo' },

    token          : { type: 'string' },

    origin         : { type: 'string' },

    destination    : { type: 'string' },

    winner         : { model: 'spin' },

    departure_date : { type: 'date' },

    return_date    : { type: 'date' },

    setToken       :  function(){
      this.token = md5(new Date().getTime());
    }

  }
};
