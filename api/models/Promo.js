/**
* Promo.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    owner             : { model:'user', required: true },

    title             : { type: 'string', required: true },

    description       : { type: 'string' },

    active            : { type: 'boolean' },

    start_date        : { type: 'date', required: true },

    end_date          : { type: 'date', required: true },

    winner_dates      : { type: 'array' },

    flight_type       : { type: 'boolean' },

    flight_start_date : { type: 'date' },

    flight_end_date   : { type: 'date' },

    flights           : { collection: 'flight', via: 'owner' },

    spins             : { collection: 'spin', via: 'owner' }

  }
};
