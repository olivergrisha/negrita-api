var User = {
  // Enforce model schema in the case of schemaless databases
  schema: true,

  attributes: {

    organization  : { type: 'string', unique: true },

    username      : { type: 'string', unique: true },

    email         : { type: 'email',  unique: true },

    passports     : { collection: 'Passport', via: 'user' },

    promos        : {
                      collection: 'promo',
                      via: 'owner'
                    }

  }
};

module.exports = User;
